package ttt;
import java.awt.*;
import java.awt.event.*;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.util.*;

public class tic_tac_toe extends JPanel{
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    char playerMark = 'X';
    JButton[] b = new JButton[n*n];
    public tic_tac_toe(){
        setLayout(new GridLayout(n,n));
        initializerButtons();
    }
    
    public void initializerButtons(){
        for(int i=0;i<n*n;i++){
			b[i]=new JButton();
            b[i].setText("-");
            b[i].setBackground(Color.white);
            b[i].addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    JButton buttonClicked=(JButton)e.getSource();
                    buttonClicked.setText(String.valueOf(playerMark));
                    
                    if(playerMark=='X'){
                        playerMark='O';
                        buttonClicked.setBackground(Color.CYAN);
                    }
                    else{
                        playerMark='X';
                        buttonClicked.setBackground(Color.PINK);
                    }
                    displayVictor();
                }
            });
            add(b[i]);
        }
    }
    public void displayVictor(){
		 if(checkForWinner()==true){
			 if(playerMark == 'O'){
				 playerMark='X';
			 }
			 else{
				 playerMark='O';
			 }
		
			 JOptionPane pane = new JOptionPane();
			 int dialogResult = JOptionPane.showConfirmDialog(pane,"GAME OVER.	"+playerMark+	"  wins.Would like to play the game again?","GAME OVER.",JOptionPane.YES_NO_OPTION);
			 if(dialogResult == JOptionPane.YES_OPTION){
				 resetButtons();
			}
			 else{
				 System.exit(0);
			 }
		 }
		 else if(checkDraw()){
			 JOptionPane pane = new JOptionPane();
			 int dialogResult = JOptionPane.showConfirmDialog(pane,"Draw.Play again?","GAME_OVER",JOptionPane.YES_NO_OPTION);
			 if(dialogResult == JOptionPane.YES_OPTION){
				 resetButtons();
			 }
			 else{
				 System.exit(0);
			 }
		 }
	 }
  public boolean checkForWinner(){
		 if(checkRows()==true || checkColumns()==true || checkDiagonals() == true)return true;
		 else return false;
  }
  public boolean checkDraw(){
		 boolean full = true;
		 for(int i=0;i<n*n;i++){
			 if(b[i].getText().charAt(0)== '-'){
				 full = false;
			 }
		 }
		 return full;
	}
   private void resetButtons(){
       for(int i=0;i<n*n;i++){
           playerMark='X';
           b[i].setText("-");
           b[i].setBackground(Color.white);
        }
    }
    public boolean checkRows(){
		 int i=0;
		 int[][]  a = new int[n][n];
		 for(int x=0;x<n;x++){
			 for(int y=0;y<n;y++){
				 b[y].getText().equals(a[x][y]);
			 }
		 }
		 for(int j=0;j<n;j++){
				if( (String.valueOf(a[i][j])=="X")  && (String.valueOf(a[i][j]).charAt(0)!='-')){
				 return true;
				}
				 else if((String.valueOf(a[i][j])=="O") && (String.valueOf(a[i][j]).charAt(0)!='-')){
					 return true;
				}
			 i=i++;
		 }
		 return false;
	 }
	 public boolean checkColumns(){
		 int j=0;
		 int[][]  a = new int[n][n];
		 for(int x=0;x<n;x++){
			 for(int y=0;y<n;y++){
				 b[x].getText().equals(a[x][y]);
			 }
		 }
		 for(int i=0;i<n;i++){
			 if( String.valueOf(a[i][j])=="X"  && String.valueOf(a[i][j]).charAt(0)!='-'){
				 return true;
			 }
   	         else if(String.valueOf(a[i][j])=="O" && String.valueOf(a[i][j]).charAt(0)!='-'){
				return true;
			  }
			j++;
		}
		return false;
	}
	public boolean checkDiagonals(){
		int[][]  a = new int[n][n];
		 for(int x=0;x<n;x++){
			 for(int y=0;y<n;y++){
				 b[x].getText().equals(a[x][y]);
			 }
		 }
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if(i==j){
					if(String.valueOf(a[i][j])=="X"){ 
						return true;
					}
					else if(String.valueOf(a[i][j])=="O"){ 
						return true;
					}
				}
				else if(i+j==(n-1)){
					if(String.valueOf(a[i][j])=="X"){ 
						return true;
					}
					else if(String.valueOf(a[i][j])=="O"){ 
						return true;
					}
				}
			}
		}
				
		return false;
	}
    public static void main(String args[]){
        JFrame window = new JFrame("TIC TAC TOE GAME");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.getContentPane().add(new tic_tac_toe());
        window.setBounds(500,500,500,500);
        window.setVisible(true);
        window.setLocationRelativeTo(null);
    }
}